package ex21;

import ex1.Animal;

public class Lion extends Animal {

    private Lionne lionne;

    public Lion(Lionne lionne) {
        this.lionne = lionne;
    }

    public void partage() {
        System.out.println("Merci, je vais manger ton " + lionne.nourriture);
    }
}