package ex2;

import ex21.Lion;
import ex21.Lionne;

public class Main {

    public static void main(String[] args) {
        Lionne lionne = new Lionne();
        lionne.nourriture = "Steak";

        Lion lion = new Lion(lionne);
        lion.partage();
    }
}
