package fr.ynov.service.exemple5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Chien extends Animal {

    @Autowired
    private Os os;

    public Os getOs() {
        return os;
    }

    public void setOs(Os os) {
        this.os = os;
    }
}