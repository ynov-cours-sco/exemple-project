package fr.ynov.service.exemple5;

import org.springframework.stereotype.Component;

@Component
public class Chien2 extends Animal {

    private Os os;

    public Chien2(Os os) {
        this.os = os;
    }

    public Os getOs() {
        return os;
    }

    public void setOs(Os os) {
        this.os = os;
    }
}