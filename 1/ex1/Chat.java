package ex1;

public class Chat extends Animal implements Mange {

    public void miaul() {
        System.out.println("Miaou");
    }

    @Override
    public void miam() {
        System.out.println("Burp");
    }
}