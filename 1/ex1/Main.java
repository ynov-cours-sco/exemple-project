package ex1;

public class Main {

    public static void main(String[] args) {
        Chien chien1 = new Chien();
        chien1.aboie();

        Chat chat1 = new Chat();
        chat1.miam();
        chat1.miaul();

        System.out.println(chat1 instanceof Animal);
    }
}
