package fr.ynov.service.exemple6.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Exemple6Controller {

    @GetMapping("/test")
    public boolean test() {
        return true;
    }
}
