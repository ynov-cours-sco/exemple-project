package fr.ynov.service.exemple6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Exemple6Application {
    public static void main(String[] args) {
        SpringApplication.run(Exemple6Application.class, args);
    }
}
